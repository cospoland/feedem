require 'test_helper'

class InterfaceControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get interface_new_url
    assert_response :success
  end

  test "should get create" do
    get interface_create_url
    assert_response :success
  end

end
