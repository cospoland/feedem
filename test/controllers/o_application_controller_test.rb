require 'test_helper'

class OApplicationControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get o_application_create_url
    assert_response :success
  end

  test "should get authorized" do
    get o_application_authorized_url
    assert_response :success
  end

  test "should get authorize" do
    get o_application_authorize_url
    assert_response :success
  end

  test "should get create_bot_user" do
    get o_application_create_bot_user_url
    assert_response :success
  end

end
