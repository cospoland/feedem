Rails.application.routes.draw do

  get 'o_application/create'

  get 'o_application/authorized'

  get 'o_application/authorize'

  get 'o_application/create_bot_user'

  get 'interface/new'

  get 'interface/create'

  # Shared data related
  get '/data/get', to: 'shareddata#pull'

  # User related
  get '/user/get/:name', to: 'user#pull'
  get '/user/list', to: 'user#pull_names'
  post '/user/update/:name', to: 'user#update'

  # Feeding related
  get '/fed/:animal', to: 'animal#count'
  post '/feed/:animal', to: 'animal#feed'

  # Registry related
  get '/registry/:animal', to: 'registry#pull'
  get '/registry/:animal/last/:max', to: 'registry#pull_max'
  get '/registry/:animal/max/:max', to: 'registry#pull_max'
  get '/registry/:animal/by-user/:user', to: 'registry#pull_by_user'
  get '/registry/:animal/by-user/:user/max/:max', to: 'registry#pull_by_user_max'
  get '/registry/:animal/by-user/:user/last/:max', to: 'registry#pull_by_user_max'

  # Auth related
  get '/callback/:provider', to: 'auth#callback'

  # Applications
  post '/application/create', to: 'o_application#create'
  get '/application/create/:appid/bot', to: 'o_application#create_bot_user'

  # SPA
  get '/', to: 'interface#serve'
end
