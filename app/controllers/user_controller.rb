class UserController < ApplicationController
  protect_from_forgery with: :null_session
  def pull
    @user = User.where(name: params[:name])
    render plain: @user[0].to_json
  end

  def update
    @token = request.headers["Authorization"].to_s.split(' ')
    if @token[0] != "Bot"
      render plain: { "error" => "only applications can use this endpoint" }.to_json
      return
    end
    @session = Session.where(token: @token[1], scope: @token[0]).select('user')
    if @session.size >= 1
      @user = User.where(userid: params[:user]).first
      while @user[:link] != nil
        @user = User.where(userid: @user[:link]).first
      end
      @appauth = OAuthorization.where(userid: "APP_SCOPE#UPDATE_POINTS", appid: @session[0][:user])
      if @appauth.size < 1
        render plain: { "error" => "application not allowed" }
        return
      end
      @appauth = OAuthorization.where(userid: @user[:userid], appid: @session[0][:user])
      if @appauth.size < 1
        render plain: { "error" => "application not authorized" }
        return
      end
      @preset = Preset.where(enabled: true).first
      @multiplier = @user['multiplier']
      @points = @preset['base_points']
      if params[:isSubscriber] == '1'
        @points += @preset['subscriber_points']
      end
      if params[:isNewSubscriber] == '1'
        @multiplier += @preset['multiplier_per_subscription']
      end
      @bonus = rand(1..@preset['bonus_chance'])
      if @bonus == 2
        @points += 1
      end
      @points *= @multiplier
      @user.update(points: @user['points'] + @points, multiplier: @multiplier)
      render plain: "{\"points\":#{@points}}"
    end
  end
end
