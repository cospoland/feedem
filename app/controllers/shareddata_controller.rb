class ShareddataController < ApplicationController
  def pull
    @preset = Preset.where(enabled: true)
    render plain: @preset.to_json
  end
end
