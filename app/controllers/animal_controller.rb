class AnimalController < ApplicationController
  protect_from_forgery with: :null_session

  def count
    
  end

  def feed
    @token = request.headers["Authorization"].to_s.split(' ')
    @session = Session.where(token: @token[1], scope: @token[0]).select('user')
    if @session.size >= 1
      @animal = Animal.where(name: params[:animal]).first
      @user = User.where(userid: params[:user]).first
      while @user[:link] != nil
        @user = User.where(userid: @user[:link]).first
      end
      @preset = Preset.where(enabled: true).first
      # check is ANIMAL param valid
      if !@animal.nil?
        # if user is nil just show insufficient points error
        if @user.nil?
          render plain: '{"error":1}'
          return
        end
        # check funds
        if @user['points'] >= @preset[@animal['cost_column_name']]
          @user.update(points: @user['points'] - @preset[@animal['cost_column_name']])
          @registry = Registry.new(animal: @animal['id'], comment: params[:comment], user: @user['userid'])
          @registry.save
          @count = Registry.where(animal: @animal['id']).count(:id)
          # no problem
          render plain: "{\"error\":0,\"result\":#{@count}}"
        else
          # insufficient points
          render plain: '{"error":1}'
        end
      else
        # invalid ANIMAL param
        render plain: '{"error":4}'
      end
    else
      render plain: '{"error":4}'
    end
  end
end
