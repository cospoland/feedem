require 'net/http'
require 'base64'
require 'json'
require 'uri'
class AuthController < ApplicationController
  protect_from_forgery with: :null_session
  def callback
    @token = request.headers["Authorization"].to_s.split(' ')
    @session = Session.where(token: @token[1]).select('user')
    @link = @session.size >= 1
    puts case params[:provider]
    when "hitbox"
      accesstoken = ""
      if params[:request_token] != nil
        req = Net::HTTP::Post.new("/oauth/exchange", initheader = 'Content-Type' => 'application/json')
        @body = {
          "request_token" => params[:request_token],
          "app_token" => ENV['HITBOX_CLIENT_ID'],
          "hash" => Base64.strict_encode64(ENV['HITBOX_CLIENT_ID'] + ENV['HITBOX_SECRET'])
        }.to_json
        req.body = @body
        http = Net::HTTP.new("api.hitbox.tv", "443")
        http.use_ssl = true
        response = http.start {|http| http.request(req) }
        if !response.body.start_with?("authentication_failed")
          accesstokentmp = JSON.parse response.body
          accesstoken = accesstokentmp['access_token']
        else
          render plain: "{\"error\":\"failed\"}"
          return
        end
      elsif params[:authToken] != nil
        accesstoken = params[:authToken]
      else
        render plain: "{\"error\":\"failed\"}"
        return
      end
      uri = URI.parse('https://api.hitbox.tv/userfromtoken/' + accesstoken)
      response = Net::HTTP.get(uri)
      username = JSON.parse response
      userid = "Hitbox#" + username['user_name']
      @authkey = ""
      if @token[1] != nil
        @authkey = @token[1]
      else
        @authkey = (0...32).map { (65 + rand(26)).chr }.join
      end
      if @link
        user = User.where(:userid => userid, :link => @session.first[:user]).first_or_create
      else
        session = Session.new(user: userid, token: @authkey, scope: "Bearer")
        session.save
        user = User.where(:userid => userid).first_or_create
      end
      render plain: { "name" => username['user_name'], "userid" => userid, "authorization" => @authkey }.to_json
    when "discord"
      if params[:error] != nil
        render plain: { "error" => params[:error] }.to_json
        return
      end
      postform = {
        "grant_type" => "authorization_code",
        "code" => params[:code],
        "redirect_uri" => ENV['DISCORD_REDIRECT_URI'],
        "client_id" => ENV['DISCORD_CLIENT_ID'],
        "client_secret" => ENV['DISCORD_SECRET']
      }
      req = Net::HTTP::Post.new("/api/oauth2/token", initheader = "Content-Type" => "application/x-www-form-urlencoded")
      req.body = URI.encode_www_form(postform)
      http = Net::HTTP.new("discordapp.com", "443")
      http.use_ssl = true
      response = http.start {|http| http.request(req) }
      authtoken = JSON.parse response.body
      req = Net::HTTP::Get.new("/api/users/@me", init_header = "Authorization" => "Bearer " + authtoken['access_token'])
      http = Net::HTTP.new("discordapp.com", "443")
      http.use_ssl = true
      response = http.start {|http| http.request(req) }
      duser = JSON.parse response.body
      userid = "Discord#" + duser['id']
      @authkey = ""
      if @token[1] != nil
        @authkey = @token[1]
      else
        @authkey = (0...32).map { (65 + rand(26)).chr }.join
      end
      if @link
        user = User.where(:userid => userid, :link => @session.first[:user]).first_or_create
      else
        session = Session.new(user: userid, token: @authkey, scope: "Bearer")
        session.save
        user = User.where(:userid => userid).first_or_create
      end
      render plain: { "name" => duser['id'], "userid" => userid, "authorization" => @authkey }.to_json
    when "twitch"
      render plain: { "error" => "not available yet" }.to_json
    when "youtube"
      render plain: { "error" => "not available yet" }.to_json
    end
  end
end
