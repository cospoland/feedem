class OApplicationController < ApplicationController
  protect_from_forgery with: :null_session
  def create
    @token = request.headers["Authorization"].to_s.split(' ')
    if @token[0] != "Bearer"
      render plain: { "error" => "only bearer sessions allowed" }.to_json
      return
    end
    if params[:app_name].nil? || params[:app_name].strip == ""
      render plain: { "error" => "specify app_name" }.to_json
      return
    end
    @session = Session.where(token: @token[1], scope: @token[0]).select('user')
    if @session.size >= 1
      @user = User.where(userid: @session[0]['user']).first
      while @user[:link] != nil
        @user = User.where(userid: @user[:link]).first
      end
      if @user.nil?
        render plain: { "error" => "no such user" }.to_json
        return
      end
      @appid = "26" + 16.times.map{rand(10)}.join
      @app = OApplication.new(appid: @appid, appname: params[:app_name], ownerid: @user['userid'])
      @app.save
      render plain: { "app_id" => @appid }.to_json
    end
  end

  def authorized
  end

  def authorize
  end

  def create_bot_user
    @token = request.headers["Authorization"].to_s.split(' ')
    if @token[0] != "Bearer"
      render plain: { "error" => "invalid scope" }.to_json
      return
    end
    if params[:appid].nil?
      render plain: { "error" => "specify appid" }.to_json
      return
    end
    @session = Session.where(token: @token[1], scope: @token[0]).select('user')
    if @session.size >= 1
      @user = User.where(userid: @session[0]['user']).first
      while @user[:link] != nil
        @user = User.where(userid: @user[:link]).first
      end
      if @user.nil?
        render plain: { "error" => "no such user" }.to_json
        return
      end
      @app = OApplication.where(appid: params[:appid], ownerid: @user['userid'])
      if @app.size == 0
        render plain: { "error" => "app does not exist" }.to_json
        return
      end
      if @app[:botid] != nil
        render plain: { "error" => "the app already has a bot user" }.to_json
      end
      @botid = "Bot#" + params[:appid]
      @botuser = User.new(userid: @botid, multiplier: 0.00)
      @botuser.save
      @app.update(botid: @botid)
      @authkey = (0...32).map { (65 + rand(26)).chr }.join
      bsession = Session.new(user: @botid, token: @authkey, scope: "Bot")
      bsession.save
      @app.update(bottoken: @authkey)
      render plain: { "app_id" => @appid, "bot_id" => @botid, "bot_token" => @authkey }.to_json
    end
  end
end
