class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :userid
      t.decimal :points, :default => 0.00, :precision => 65, :scale => 3
      t.decimal :multiplier, :precision => 3, :scale => 2, :default => 1.00
      t.boolean :admin
      t.string :link

      t.timestamps
    end
  end
end
