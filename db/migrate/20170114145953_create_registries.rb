class CreateRegistries < ActiveRecord::Migration[5.0]
  def change
    create_table :registries do |t|
      t.string :user
      t.integer :animal
      t.string :comment, :default => ""

      t.timestamps
    end
  end
end
