class CreateIntegrations < ActiveRecord::Migration[5.0]
  def change
    create_table :integrations do |t|
      t.string :provider
      t.string :userid
      t.string :name

      t.timestamps
    end
  end
end
