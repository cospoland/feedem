class CreateOAuthorizations < ActiveRecord::Migration[5.0]
  def change
    create_table :o_authorizations do |t|
      t.string :ownerid
      t.string :appid
      t.integer :scopes

      t.timestamps
    end
  end
end
