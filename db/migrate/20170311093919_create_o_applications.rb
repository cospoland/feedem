class CreateOApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :o_applications do |t|
      t.string :appid
      t.string :appname
      t.string :ownerid
      t.string :botid
      t.string :bottoken

      t.timestamps
    end
  end
end
