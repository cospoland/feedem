class CreatePresets < ActiveRecord::Migration[5.0]
  def change
    create_table :presets do |t|
      t.decimal :hay_cost, :precision => 30, :scale => 3
      t.decimal :bamboo_cost, :precision => 30, :scale => 3
      t.integer :bonus_chance
      t.decimal :subscriber_points, :precision => 30, :scale => 3
      t.decimal :base_points, :precision => 30, :scale => 3
      t.decimal :multiplier_per_subscription, :precision => 2, :scale => 2
      t.boolean :enabled, :default => false
      t.string :comment, :default => ""

      t.timestamps
    end
  end
end
