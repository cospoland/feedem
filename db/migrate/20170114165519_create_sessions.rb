class CreateSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :sessions do |t|
      t.string :user
      t.string :scope
      t.string :token
      t.string :hitbox

      t.timestamps
    end
  end
end
