# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170317175844) do

  create_table "animals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.string   "cost_column_name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "integrations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "provider"
    t.string   "userid"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "o_applications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "appid"
    t.string   "appname"
    t.string   "ownerid"
    t.string   "botid"
    t.string   "bottoken"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "o_authorizations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "ownerid"
    t.string   "appid"
    t.integer  "scopes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "presets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.decimal  "hay_cost",                    precision: 30, scale: 3
    t.decimal  "bamboo_cost",                 precision: 30, scale: 3
    t.integer  "bonus_chance"
    t.decimal  "subscriber_points",           precision: 30, scale: 3
    t.decimal  "base_points",                 precision: 30, scale: 3
    t.decimal  "multiplier_per_subscription", precision: 2,  scale: 2
    t.boolean  "enabled",                                              default: false
    t.string   "comment",                                              default: ""
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
  end

  create_table "registries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "user"
    t.integer  "animal"
    t.string   "comment",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "sessions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "user"
    t.string   "scope"
    t.string   "token"
    t.string   "hitbox"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "userid"
    t.decimal  "points",     precision: 65, scale: 3, default: "0.0"
    t.decimal  "multiplier", precision: 3,  scale: 2, default: "1.0"
    t.boolean  "admin"
    t.string   "link"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

end
